<?php namespace Dybo\Casaautomecca\Models;

use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dybo_casaautomecca_products';

    public $belongsTo = [
        'category' => 'dybo\casaautomecca\models\Category'
    ];
}
