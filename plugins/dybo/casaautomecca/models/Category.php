<?php namespace Dybo\Casaautomecca\Models;

use Model;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dybo_casaautomecca_categories';

    public $hasMany = [
        'products' => 'dybo\casaautomecca\models\Product'
    ];
}
