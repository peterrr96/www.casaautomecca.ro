<?php namespace Dybo\Casaautomecca\Components;

use Cms\Classes\ComponentBase;

use Dybo\Casaautomecca\Models\Product;

class ProductsFilter extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Lista vehicule',
            'description' => 'Lista produselor/vehiculelor'
        ];
    }

    public function onRun()
    {
        $this->products = $this->page['products'] = $this->loadProducts();
    }


    protected function loadProducts()

    {
        $parent = $this->param('parent');
        $uri = $this->param('subparent');
        $product_uri = $this->param('product');
        $page = $this->param('page');
        if($product_uri == null){
                if($uri == null){
                $products = Product::whereHas('category', function($query) use ($parent,$uri){
                    $query->where('parent', $parent);
                })->paginate(9,$page);
            }
            else{
                $products = Product::whereHas('category', function($query) use ($parent,$uri){
                $query->where('parent', $parent)->where('uri', $uri);
            })->paginate(9,$page);
            }

            return $products;
        }
        else {
            $product = Product::where('uri', $product_uri)->first();
            return $product;
        }
        
       
        // dd($products->lastPage());
        
        
    }
    
    public $products;

}