<?php namespace Dybo\Casaautomecca\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDyboCasaautomeccaCategories extends Migration
{
    public function up()
    {
        Schema::table('dybo_casaautomecca_categories', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('dybo_casaautomecca_categories', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
