<?php namespace Dybo\Casaautomecca\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDyboCasaautomeccaProducts extends Migration
{
    public function up()
    {
        Schema::table('dybo_casaautomecca_products', function($table)
        {
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('dybo_casaautomecca_products', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
