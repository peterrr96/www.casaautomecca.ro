<?php namespace Dybo\Casaautomecca\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDyboCasaautomeccaProducts extends Migration
{
    public function up()
    {
        Schema::create('dybo_casaautomecca_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('name');

            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('dybo_casaautomecca_categories');

            $table->text('image')->nullable();
            $table->text('uri');
            $table->text('alt')->nullable();
            $table->text('description')->nullable();
            
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dybo_casaautomecca_products');
    }
}
