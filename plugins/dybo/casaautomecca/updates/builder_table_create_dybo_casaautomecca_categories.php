<?php namespace Dybo\Casaautomecca\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDyboCasaautomeccaCategories extends Migration
{
    public function up()
    {
        Schema::create('dybo_casaautomecca_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('name');
            $table->text('uri');
            $table->text('parent');
            
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dybo_casaautomecca_categories');
    }
}
