<?php namespace Dybo\Casaautomecca;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Dybo\Casaautomecca\Components\ProductsFilter' => 'products'
        ];
    }

    public function registerSettings()
    {
    }
}
