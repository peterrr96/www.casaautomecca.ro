/*
 * Application
 */

$(document).tooltip({
    selector: "[data-toggle=tooltip]"
})

/*
 * Auto hide navbar
 */
jQuery(document).ready(function($){
    var $header = $('.navbar-autohide'),
        scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 150

    $(window).on('scroll', function(){
        if (!scrolling) {
            scrolling = true

            if (!window.requestAnimationFrame) {
                setTimeout(autoHideHeader, 250)
            }
            else {
                requestAnimationFrame(autoHideHeader)
            }
        }
    })

    function autoHideHeader() {
        var currentTop = $(window).scrollTop()

        // Scrolling up
        if (previousTop - currentTop > scrollDelta) {
            $header.removeClass('is-hidden')
        }
        else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
            // Scrolling down
            $header.addClass('is-hidden')
        }

        previousTop = currentTop
        scrolling = false
    }
});


$(function(){
    $("#map").click(function(){
        $(this).find("iframe").css( "pointer-events", "auto" );
    });

    $('.carousel').carousel({
        interval: 3000,
        keyboard: true
    });
});


$("#toTop").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});
$("#toTop").hide()
$(window).scroll(function() {
    if ($(window).scrollTop() > 100) {

        $("#toTop").show();
    }
    else {

        $("#toTop").hide();
    }
});

$('.jvalidate').validate({
    submitHandler: function(form){
        var submitter = $(form).find('button');
        var data = $(form).serialize();
        var html = '<div class="text-center"><i class="fa fa-check fa-2x"></i><br/><b>Mesaj transmis cu succes!</b><br/>Multumim!</div>';

        submitter.html('<i class="fa fa-spin fa-refresh"></i> Se trimite');
        submitter.prop('disabled', true);

        $.post(
            'ajax/send.php',
            data,
            function(response){
                $(form).replaceWith(html);
            }
        )
    }
});
