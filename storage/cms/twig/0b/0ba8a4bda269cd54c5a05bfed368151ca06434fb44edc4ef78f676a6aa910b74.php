<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/pages/home.htm */
class __TwigTemplate_95012b92e98b2fd0ae4ef9177cb7d98138260c055120111609c870d9ebf675d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"main-slider\" class=\"carousel slide\" data-ride=\"carousel\">


    <!-- Wrapper for slides -->
    <div class=\"carousel-inner\" role=\"listbox\">

        <div class=\"item active\">
            <a href=\"atv-quad\" title=\"\" >
                <img src=\"";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/banners/motor-sport.png");
        echo "\" style=\"display: block; margin: auto;\" alt=\"\" >
            </a>
        </div>

        <div class=\"item \">

            <img src=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/banners/pedal-box-ploiesti.jpg");
        echo "\" style=\"display: block; margin: auto;\" alt=\"\" >

        </div>

        <div class=\"item \">
            <a href=\"despre-instalatiile-gpl\" title=\"\" >
                <img src=\"";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/banners/banner_gpl.jpg");
        echo "\" style=\"display: block; margin: auto;\" alt=\"\" >
            </a>
        </div>

        <div class=\"item \">

            <img src=\"";
        // line 27
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/banners/banner_600.jpg");
        echo "\" style=\"display: block; margin: auto;\" alt=\"\" >

        </div>
    </div>

    <!-- Controls -->
    <a class=\"left carousel-control\" href=\"#main-slider\" role=\"button\" data-slide=\"prev\">
        <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
        <span class=\"sr-only\">Previous</span>
    </a>
    <a class=\"right carousel-control\" href=\"#main-slider\" role=\"button\" data-slide=\"next\">
        <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
        <span class=\"sr-only\">Next</span>
    </a>

    <!-- Indicators -->
    <ol class=\"carousel-indicators\">

        <li data-target=\"#main-slider\" data-slide-to=\"0\" class=\"active\"></li>

        <li data-target=\"#main-slider\" data-slide-to=\"1\" ></li>

        <li data-target=\"#main-slider\" data-slide-to=\"2\" ></li>

        <li data-target=\"#main-slider\" data-slide-to=\"3\" ></li>
    </ol>
</div>


<div class=\"container\">
    <div class=\"col-sm-2 hidden\" id=\"quick-menu\">
        <ul class=\"list-unstyled\">
            <li>
                <a href=\"\">
                    <button class=\"btn btn-info\">
                        <i class=\"fa fa-wrench fa-2x\"></i>
                        <br/>
                        Programare<br/>service
                    </button>
                </a>
            </li>
            <li>
                <a href=\"contact\">
                    <button class=\"btn btn-info\">
                        <i class=\"fa fa-envelope fa-2x\"></i>
                        <br/>
                        Contact
                    </button>
                </a>
            </li>
            <li>
                <a href=\"\">
                    <button class=\"btn btn-info\">
                        <i class=\"fa fa-dollar fa-2x\"></i>
                        <br/>
                        Cere oferta
                    </button>
                </a>
            </li>
        </ul>
    </div>

    <div class=\"clearfix\">&nbsp;</div>
</div>

<div class=\"clearfix\">&nbsp;</div>
<div class=\"clearfix\">&nbsp;</div>

<div class=\"container\">

    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"mecanica-electrica-multimarca\">
            <h3>Mecanică</h3>
            <img src=\"";
        // line 100
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/mecanica-auto.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"tinichigerie-vopsitorie-multimarca\">
            <h3>Tinichigerie</h3>
            <img src=\"";
        // line 106
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/tinichigerie.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"anvelope-jante-auto\">
            <h3>Anvelope</h3>
            <img src=\"";
        // line 112
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/anvelope-jante-auto.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"piese-si-accesorii-auto\">
            <h3>Piese</h3>
            <img src=\"";
        // line 118
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/piese-de-schimb.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"linie-itp\">
            <h3>ITP</h3>
            <img src=\"";
        // line 124
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/itp.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"despre-instalatiile-gpl\">
            <h3>GPL</h3>
            <img src=\"";
        // line 130
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/gpl.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-6 col-xs-12\">
        <a class=\"boxy\" href=\"http://www.hyundaiploiesti.ro\" target=\"_blank\">
            <h3>Reprezentanță</h3>
            <img src=\"";
        // line 136
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/hyundai.png");
        echo "\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-6 col-xs-12\">
        <a class=\"boxy\" href=\"atv-quad\">
            <h3>Motosport</h3>
            <img src=\"";
        // line 142
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/mecanica-auto/moto.png");
        echo "\" alt=\"\"/>
        </a>
    </div>

    <div class=\"clearfix\"></div>
</div>

<div class=\"clearfix\">&nbsp;</div>

<div class=\"text-center\" id=\"map-title\">
    <h3 class=\"text-center\">Unde ne găsiți?</h3>
</div>

<div id=\"map\">
    <iframe src=\"https://www.google.com/maps/embed?pb=1m14!1m8!1m3!1d2823.804061707053!2d26.03742!3d44.947651!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9a29ef4ab2b92384!2sCasa+Auto+Mecca!5e0!3m2!1sen!2sus!4v1473424969499\" width=\"100%\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
</div>


<div class=\"clearfix\"></div>







<a href=\"atv-quad\"><img src=\"";
        // line 168
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/banner_motorsport.png");
        echo "\" alt=\"\"/></a>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 168,  195 => 142,  186 => 136,  177 => 130,  168 => 124,  159 => 118,  150 => 112,  141 => 106,  132 => 100,  56 => 27,  47 => 21,  38 => 15,  29 => 9,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"main-slider\" class=\"carousel slide\" data-ride=\"carousel\">


    <!-- Wrapper for slides -->
    <div class=\"carousel-inner\" role=\"listbox\">

        <div class=\"item active\">
            <a href=\"atv-quad\" title=\"\" >
                <img src=\"{{'assets/images/banners/motor-sport.png'|theme}}\" style=\"display: block; margin: auto;\" alt=\"\" >
            </a>
        </div>

        <div class=\"item \">

            <img src=\"{{'assets/images/banners/pedal-box-ploiesti.jpg'|theme}}\" style=\"display: block; margin: auto;\" alt=\"\" >

        </div>

        <div class=\"item \">
            <a href=\"despre-instalatiile-gpl\" title=\"\" >
                <img src=\"{{'assets/images/banners/banner_gpl.jpg'|theme}}\" style=\"display: block; margin: auto;\" alt=\"\" >
            </a>
        </div>

        <div class=\"item \">

            <img src=\"{{'assets/images/banners/banner_600.jpg'|theme}}\" style=\"display: block; margin: auto;\" alt=\"\" >

        </div>
    </div>

    <!-- Controls -->
    <a class=\"left carousel-control\" href=\"#main-slider\" role=\"button\" data-slide=\"prev\">
        <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
        <span class=\"sr-only\">Previous</span>
    </a>
    <a class=\"right carousel-control\" href=\"#main-slider\" role=\"button\" data-slide=\"next\">
        <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
        <span class=\"sr-only\">Next</span>
    </a>

    <!-- Indicators -->
    <ol class=\"carousel-indicators\">

        <li data-target=\"#main-slider\" data-slide-to=\"0\" class=\"active\"></li>

        <li data-target=\"#main-slider\" data-slide-to=\"1\" ></li>

        <li data-target=\"#main-slider\" data-slide-to=\"2\" ></li>

        <li data-target=\"#main-slider\" data-slide-to=\"3\" ></li>
    </ol>
</div>


<div class=\"container\">
    <div class=\"col-sm-2 hidden\" id=\"quick-menu\">
        <ul class=\"list-unstyled\">
            <li>
                <a href=\"\">
                    <button class=\"btn btn-info\">
                        <i class=\"fa fa-wrench fa-2x\"></i>
                        <br/>
                        Programare<br/>service
                    </button>
                </a>
            </li>
            <li>
                <a href=\"contact\">
                    <button class=\"btn btn-info\">
                        <i class=\"fa fa-envelope fa-2x\"></i>
                        <br/>
                        Contact
                    </button>
                </a>
            </li>
            <li>
                <a href=\"\">
                    <button class=\"btn btn-info\">
                        <i class=\"fa fa-dollar fa-2x\"></i>
                        <br/>
                        Cere oferta
                    </button>
                </a>
            </li>
        </ul>
    </div>

    <div class=\"clearfix\">&nbsp;</div>
</div>

<div class=\"clearfix\">&nbsp;</div>
<div class=\"clearfix\">&nbsp;</div>

<div class=\"container\">

    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"mecanica-electrica-multimarca\">
            <h3>Mecanică</h3>
            <img src=\"{{'assets/images/mecanica-auto/mecanica-auto.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"tinichigerie-vopsitorie-multimarca\">
            <h3>Tinichigerie</h3>
            <img src=\"{{'assets/images/mecanica-auto/tinichigerie.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"anvelope-jante-auto\">
            <h3>Anvelope</h3>
            <img src=\"{{'assets/images/mecanica-auto/anvelope-jante-auto.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"piese-si-accesorii-auto\">
            <h3>Piese</h3>
            <img src=\"{{'assets/images/mecanica-auto/piese-de-schimb.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"linie-itp\">
            <h3>ITP</h3>
            <img src=\"{{'assets/images/mecanica-auto/itp.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
        <a class=\"boxy\" href=\"despre-instalatiile-gpl\">
            <h3>GPL</h3>
            <img src=\"{{'assets/images/mecanica-auto/gpl.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-6 col-xs-12\">
        <a class=\"boxy\" href=\"http://www.hyundaiploiesti.ro\" target=\"_blank\">
            <h3>Reprezentanță</h3>
            <img src=\"{{'assets/images/mecanica-auto/hyundai.png'|theme}}\" alt=\"\"/>
        </a>
    </div>
    <div class=\"col-md-3 col-sm-6 col-xs-12\">
        <a class=\"boxy\" href=\"atv-quad\">
            <h3>Motosport</h3>
            <img src=\"{{'assets/images/mecanica-auto/moto.png'|theme}}\" alt=\"\"/>
        </a>
    </div>

    <div class=\"clearfix\"></div>
</div>

<div class=\"clearfix\">&nbsp;</div>

<div class=\"text-center\" id=\"map-title\">
    <h3 class=\"text-center\">Unde ne găsiți?</h3>
</div>

<div id=\"map\">
    <iframe src=\"https://www.google.com/maps/embed?pb=1m14!1m8!1m3!1d2823.804061707053!2d26.03742!3d44.947651!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9a29ef4ab2b92384!2sCasa+Auto+Mecca!5e0!3m2!1sen!2sus!4v1473424969499\" width=\"100%\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
</div>


<div class=\"clearfix\"></div>







<a href=\"atv-quad\"><img src=\"{{'assets/images/banner_motorsport.png'|theme}}\" alt=\"\"/></a>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/home.htm", "");
    }
}
