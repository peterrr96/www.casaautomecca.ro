<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/partials/site/footer.htm */
class __TwigTemplate_682cc83b0bd02dcaf0b9da35337d28d3a91ec21d3784fa62e611ccf8dd64a77a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer\">
        <div class=\"container\">
            <div class=\"col-sm-4 text-center\" >
                <p class=\"text-center\" style=\"font-style: italic; padding-top: 10px;\"><b>ECHIPA NOASTRĂ TE AȘTEAPTĂ:</b><br>
                    Ploiești, Str. Apelor 2 (lângă Artsani și AFI Mall)<br>
                    Tel/fax: <a href=\"tel:0244 515 736\" style=\"color: #fff; text-decoration: none;\">0244 515 736</a><br>
                    <a href=\"mailto:contact@casaautomecca.ro\" style=\"color: #fff; text-decoration: none;\">contact@casaautomecca.ro</a>
                </p>
            </div>

            <div class=\"col-sm-4 text-center\">
                <img src=\"";
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo2.png");
        echo "\" alt=\"\" />
            </div>

            <div class=\"col-sm-4 text-center\">
                <p id=\"news\">Afla primul noutățile!</p>

                <input class=\"form-control\" type=\"email\" name=\"email\" required placeholder=\"email\">

                <button class=\"btn btn-default\">Înscriere</button>
            </div>
        </div>
    </div>
    <a href=\"#\" id=\"toTop\" class=\"text-center\">TOP</a>

    <div style=\"padding: 5px;\" class=\"text-center\">
        Website realizat de <a href=\"http://www.eastwest.ro/\" target=\"_blank\">EastWest</a> si <a href=\"http://www.dybo.ro/\" target=\"_blank\">Dybo</a>
    </div>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 12,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"footer\">
        <div class=\"container\">
            <div class=\"col-sm-4 text-center\" >
                <p class=\"text-center\" style=\"font-style: italic; padding-top: 10px;\"><b>ECHIPA NOASTRĂ TE AȘTEAPTĂ:</b><br>
                    Ploiești, Str. Apelor 2 (lângă Artsani și AFI Mall)<br>
                    Tel/fax: <a href=\"tel:0244 515 736\" style=\"color: #fff; text-decoration: none;\">0244 515 736</a><br>
                    <a href=\"mailto:contact@casaautomecca.ro\" style=\"color: #fff; text-decoration: none;\">contact@casaautomecca.ro</a>
                </p>
            </div>

            <div class=\"col-sm-4 text-center\">
                <img src=\"{{'assets/images/logo2.png'|theme}}\" alt=\"\" />
            </div>

            <div class=\"col-sm-4 text-center\">
                <p id=\"news\">Afla primul noutățile!</p>

                <input class=\"form-control\" type=\"email\" name=\"email\" required placeholder=\"email\">

                <button class=\"btn btn-default\">Înscriere</button>
            </div>
        </div>
    </div>
    <a href=\"#\" id=\"toTop\" class=\"text-center\">TOP</a>

    <div style=\"padding: 5px;\" class=\"text-center\">
        Website realizat de <a href=\"http://www.eastwest.ro/\" target=\"_blank\">EastWest</a> si <a href=\"http://www.dybo.ro/\" target=\"_blank\">Dybo</a>
    </div>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/partials/site/footer.htm", "");
    }
}
