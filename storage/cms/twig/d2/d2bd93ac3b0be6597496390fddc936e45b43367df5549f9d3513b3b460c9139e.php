<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/partials/site/header.htm */
class __TwigTemplate_cf64e6f2a9f3d331015c8b7b6f8e6cb8a78127a90195c8db6f352df1aaf7a8bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header>
    <div class=\"container-fluid\" id=\"top\">

        <a href=\"tel:+40244 515 736\"><i class=\"fa fa-phone-square fa-2x\"></i> 0244 515 736 </a>
        <a href=\"mailto:contact@casaautomecca.ro\"><i class=\"fa fa-envelope-square fa-2x\"></i>
            contact@casaautomecca.ro</a>
        <a href=\"https://www.facebook.com/casaautomecca\" target=\"_blank\" class=\"hidden-sm hidden-md hidden-lg\"><i
                class=\"fa fa-facebook-square fa-2x\"></i> casaautomecca</a>
        <a href=\"https://www.facebook.com/motosportploiesti\" target=\"_blank\" class=\"hidden-sm hidden-md hidden-lg\"><i
                class=\"fa fa-facebook-square fa-2x\"></i> motosportploiesti</a>

        <div class=\"pull-right hidden-xs\">
            <ul class=\"list-inline\" id=\"top-menu\">
                <li>
                    <a href=\"despre-noi\" title=\"Despre noi\"
                    >
                        Despre noi
                    </a>
                </li>
                <li>
                    <a href=\"contact\" title=\"Contact\"
                    >
                        Contact
                    </a>
                </li>
                <li class=\"facebook\">
                    <a href=\"https://www.facebook.com/casaautomecca\" target=\"_blank\">
                        <i class=\"fa fa-facebook-square fa-2x\"></i> Casa Mecca
                    </a>
                </li>
                <li class=\"facebook\">
                    <a href=\"https://www.facebook.com/motosportploiesti\" target=\"_blank\">
                        <i class=\"fa fa-facebook-square fa-2x\"></i> MotoSport
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class=\"container-fluid text-center\" id=\"top-logo\">
        <a href=\"http://www.casaautomecca.ro/\"><img src=\"";
        // line 41
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo1.png");
        echo "\" alt=\"\"></a>
    </div>


    <nav class=\"navbar navbar-default\" id=\"mainMenu\">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <!--                <a class=\"navbar-brand\" href=\"#\">Brand</a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse text-center\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">

                <li class=\"visible-xs\">&nbsp;</li>
                <li><a href=\"despre-noi\" class=\"visible-xs\"><i class=\"fa fa-user\"></i> DESPRE NOI</a></li>
                <li><a href=\"promotii\" class=\"visible-xs\"><i class=\"fa fa-certificate\"></i> PROMOTII</a></li>
                <li class=\"visible-xs\">
                    <hr/>
                </li>
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"
                       role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        SERVICE AUTO <span class=\"caret\"></span>
                    </a>

                    <ul class=\"dropdown-menu\">
                        <li>
                            <a href=\"mecanica-electrica-multimarca\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Mecanică - electrică multimarcă
                            </a>
                        </li>
                        <li>
                            <a href=\"tinichigerie-vopsitorie-multimarca\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Tinichigerie - vopsitorie multimarcă
                            </a>
                        </li>
                        <li role=\"separator\" class=\"divider\"></li>

                        <li>
                            <a href=\"anvelope-jante-auto\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Anvelope și jante auto
                            </a>
                        </li>
                        <li>
                            <a href=\"piese-si-accesorii-auto\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Piese și accesorii auto
                            </a>
                        </li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li>
                            <a href=\"linie-itp\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Linie ITP
                            </a>
                        </li>
                    </ul>
                </li>
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"
                       role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        INSTALAȚII GPL <span class=\"caret\"></span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li>
                            <a href=\"despre-instalatiile-gpl\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Despre instalațiile GPL
                            </a>
                        </li>
                        <li>
                            <a href=\"intrebari-frecvente\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Întrebari frecvente
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=\"http://www.hyundaiploiesti.ro\" target=\"_blank\">HYUNDAI</a>
                </li>

                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"
                       role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        MOTOSPORT <span class=\"caret\"></span>
                    </a>

                    <ul class=\"dropdown-menu\">
                        <li>
                            <a href=\"atv-quad\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                ATV & Quad
                            </a>
                        </li>
                        <li>
                            <a href=\"side-by-side\">
                                <i class=\"fa fa-caret-right\"></i>
                                Side-by-side
                            </a>
                        </li>
                        <li>
                            <a href=\"snowmobile\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Snowmobile
                            </a>
                        </li>
                        <li>
                            <a href=\"jetski\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Jetski
                            </a>
                        </li>
                        <li>
                            <a href=\"accesorii-anvelope\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Accesorii si Anvelope
                            </a>
                        </li>
                        <li>
                            <a href=\"echipamente\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Echipamente
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=\"promotii\" title=\"Promoții\"
                    >
                        <b style=\"color: #6D0431;\">PROMOȚII</b>
                    </a>
                </li>
                <li class=\"visible-xs\">
                    <hr/>
                </li>
                <li class=\"visible-xs\">
                    <a href=\"contact\"><i class=\"fa fa-envelope\"></i> CONTACT</a>
                </li>
                <li class=\"visible-xs\">&nbsp;</li>

            </ul>
        </div>
    </nav>

    <div class=\"container-fluid text-center\" id=\"hyundai\">
        <p>Dealer PREMIUM <a href=\"http://www.hyundaiploiesti.ro/\" target=\"_blank\"><img src=\"";
        // line 211
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/hyundai.png");
        echo "\"
                                                                                        style=\"width: 140px; vertical-align: middle;\"></a>
        </p>
    </div>

    <div class=\"clearfix\">&nbsp;</div>


</header>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 211,  61 => 41,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header>
    <div class=\"container-fluid\" id=\"top\">

        <a href=\"tel:+40244 515 736\"><i class=\"fa fa-phone-square fa-2x\"></i> 0244 515 736 </a>
        <a href=\"mailto:contact@casaautomecca.ro\"><i class=\"fa fa-envelope-square fa-2x\"></i>
            contact@casaautomecca.ro</a>
        <a href=\"https://www.facebook.com/casaautomecca\" target=\"_blank\" class=\"hidden-sm hidden-md hidden-lg\"><i
                class=\"fa fa-facebook-square fa-2x\"></i> casaautomecca</a>
        <a href=\"https://www.facebook.com/motosportploiesti\" target=\"_blank\" class=\"hidden-sm hidden-md hidden-lg\"><i
                class=\"fa fa-facebook-square fa-2x\"></i> motosportploiesti</a>

        <div class=\"pull-right hidden-xs\">
            <ul class=\"list-inline\" id=\"top-menu\">
                <li>
                    <a href=\"despre-noi\" title=\"Despre noi\"
                    >
                        Despre noi
                    </a>
                </li>
                <li>
                    <a href=\"contact\" title=\"Contact\"
                    >
                        Contact
                    </a>
                </li>
                <li class=\"facebook\">
                    <a href=\"https://www.facebook.com/casaautomecca\" target=\"_blank\">
                        <i class=\"fa fa-facebook-square fa-2x\"></i> Casa Mecca
                    </a>
                </li>
                <li class=\"facebook\">
                    <a href=\"https://www.facebook.com/motosportploiesti\" target=\"_blank\">
                        <i class=\"fa fa-facebook-square fa-2x\"></i> MotoSport
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class=\"container-fluid text-center\" id=\"top-logo\">
        <a href=\"http://www.casaautomecca.ro/\"><img src=\"{{'assets/images/logo1.png'|theme}}\" alt=\"\"></a>
    </div>


    <nav class=\"navbar navbar-default\" id=\"mainMenu\">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <!--                <a class=\"navbar-brand\" href=\"#\">Brand</a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse text-center\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">

                <li class=\"visible-xs\">&nbsp;</li>
                <li><a href=\"despre-noi\" class=\"visible-xs\"><i class=\"fa fa-user\"></i> DESPRE NOI</a></li>
                <li><a href=\"promotii\" class=\"visible-xs\"><i class=\"fa fa-certificate\"></i> PROMOTII</a></li>
                <li class=\"visible-xs\">
                    <hr/>
                </li>
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"
                       role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        SERVICE AUTO <span class=\"caret\"></span>
                    </a>

                    <ul class=\"dropdown-menu\">
                        <li>
                            <a href=\"mecanica-electrica-multimarca\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Mecanică - electrică multimarcă
                            </a>
                        </li>
                        <li>
                            <a href=\"tinichigerie-vopsitorie-multimarca\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Tinichigerie - vopsitorie multimarcă
                            </a>
                        </li>
                        <li role=\"separator\" class=\"divider\"></li>

                        <li>
                            <a href=\"anvelope-jante-auto\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Anvelope și jante auto
                            </a>
                        </li>
                        <li>
                            <a href=\"piese-si-accesorii-auto\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Piese și accesorii auto
                            </a>
                        </li>
                        <li role=\"separator\" class=\"divider\"></li>
                        <li>
                            <a href=\"linie-itp\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Linie ITP
                            </a>
                        </li>
                    </ul>
                </li>
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"
                       role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        INSTALAȚII GPL <span class=\"caret\"></span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li>
                            <a href=\"despre-instalatiile-gpl\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Despre instalațiile GPL
                            </a>
                        </li>
                        <li>
                            <a href=\"intrebari-frecvente\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Întrebari frecvente
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=\"http://www.hyundaiploiesti.ro\" target=\"_blank\">HYUNDAI</a>
                </li>

                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"
                       role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        MOTOSPORT <span class=\"caret\"></span>
                    </a>

                    <ul class=\"dropdown-menu\">
                        <li>
                            <a href=\"atv-quad\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                ATV & Quad
                            </a>
                        </li>
                        <li>
                            <a href=\"side-by-side\">
                                <i class=\"fa fa-caret-right\"></i>
                                Side-by-side
                            </a>
                        </li>
                        <li>
                            <a href=\"snowmobile\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Snowmobile
                            </a>
                        </li>
                        <li>
                            <a href=\"jetski\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Jetski
                            </a>
                        </li>
                        <li>
                            <a href=\"accesorii-anvelope\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Accesorii si Anvelope
                            </a>
                        </li>
                        <li>
                            <a href=\"echipamente\"
                            >
                                <i class=\"fa fa-caret-right\"></i>
                                Echipamente
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=\"promotii\" title=\"Promoții\"
                    >
                        <b style=\"color: #6D0431;\">PROMOȚII</b>
                    </a>
                </li>
                <li class=\"visible-xs\">
                    <hr/>
                </li>
                <li class=\"visible-xs\">
                    <a href=\"contact\"><i class=\"fa fa-envelope\"></i> CONTACT</a>
                </li>
                <li class=\"visible-xs\">&nbsp;</li>

            </ul>
        </div>
    </nav>

    <div class=\"container-fluid text-center\" id=\"hyundai\">
        <p>Dealer PREMIUM <a href=\"http://www.hyundaiploiesti.ro/\" target=\"_blank\"><img src=\"{{'assets/images/hyundai.png'|theme}}\"
                                                                                        style=\"width: 140px; vertical-align: middle;\"></a>
        </p>
    </div>

    <div class=\"clearfix\">&nbsp;</div>


</header>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/partials/site/header.htm", "");
    }
}
