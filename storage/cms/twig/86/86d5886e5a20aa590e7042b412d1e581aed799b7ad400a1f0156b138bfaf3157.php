<?php

/* E:\Dyborian1\www.casaautomecca.ro/plugins/dybo/casaautomecca/components/productsfilter/default.htm */
class __TwigTemplate_433b516db7fecf0450ca5e0faa714ff7387758fd90479dec06700519e9902677 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["products"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["__SELF__"] ?? null), "products", array());
        // line 2
        echo "

    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 5
            echo "        <div class=\"col-sm-4\"> 
            <a class=\"product\" href=\"";
            // line 6
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["product"], "category", array()), "parent", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["product"], "category", array()), "uri", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["product"], "uri", array()), "html", null, true);
            echo "\">
                <div class=\"img-holder\">
                    <img src=\"";
            // line 8
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/products");
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["product"], "image", array()), "html", null, true);
            echo "\">
                </div>
                <div class=\"prod-name\">
                    ";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["product"], "name", array()), "html", null, true);
            echo "
                </div>
            </a>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "




";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/plugins/dybo/casaautomecca/components/productsfilter/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 16,  49 => 11,  41 => 8,  32 => 6,  29 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set products = __SELF__.products %}


    {% for product in products %}
        <div class=\"col-sm-4\"> 
            <a class=\"product\" href=\"{{product.category.parent}}/{{product.category.uri}}/{{product.uri}}\">
                <div class=\"img-holder\">
                    <img src=\"{{'assets/images/products'|theme}}/{{product.image}}\">
                </div>
                <div class=\"prod-name\">
                    {{product.name}}
                </div>
            </a>
        </div>
    {% endfor %}





", "E:\\Dyborian1\\www.casaautomecca.ro/plugins/dybo/casaautomecca/components/productsfilter/default.htm", "");
    }
}
