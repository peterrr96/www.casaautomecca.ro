<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/pages/produs.htm */
class __TwigTemplate_a7c695e24a1a250a789d0c631ddbe70901c3c2d183987d9f3a142394e5ebf982 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container text-center\">
    <h1>";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "name", array()), "html", null, true);
        echo "</h1>
    <div class=\"col-lg-6\">
        <img class=\"img-responsive\" src=\"";
        // line 4
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/products");
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "image", array()), "html", null, true);
        echo "\">
    </div>
    <div class=\"col-lg-6\">
    <p>";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "description", array()), "html", null, true);
        echo "</p>
    </div>
    
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/produs.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 7,  27 => 4,  22 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container text-center\">
    <h1>{{products.name}}</h1>
    <div class=\"col-lg-6\">
        <img class=\"img-responsive\" src=\"{{'assets/images/products'|theme}}/{{products.image}}\">
    </div>
    <div class=\"col-lg-6\">
    <p>{{products.description}}</p>
    </div>
    
</div>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/produs.htm", "");
    }
}
