<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/pages/produse-uri.htm */
class __TwigTemplate_6167e7ffd2731b54260fcd460301ff1b3e2722991dcd7d0a08700513582cd69c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["categories"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "records", array());
        // line 2
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div class=\"clearfix\">&nbsp;</div>

            <ul class=\"list-unstyled\" id=\"sidebar_subcategs\">
                <li>
                    <a href=\"atv-quad/can-am/1\">ATV Can-Am
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/cf-moto/1\">ATV CF Moto
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-honda/1\">ATV Honda
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-kawasaki/1\">ATV Kawasaki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-linhai/1\">ATV Linhai
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/polaris/1\">ATV Polaris
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-suzuki/1\">ATV Suzuki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-yamaha/1\">ATV Yamaha
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/can-am-ssv/1\">Can-Am SSV
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class=\"col-md-9\">
            <h1>ATV & Quad</h1>
            <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                    
                        ";
        // line 61
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 2)) {
            // line 62
            echo "                        <li>
                            <a href=\"";
            // line 63
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => 1));
            echo "\"><<</a>
                        </li>
                        ";
        }
        // line 66
        echo "                        
                        ";
        // line 67
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 1)) {
            // line 68
            echo "                        <li>
                            <a href=\"";
            // line 69
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) - 1)));
            echo "\"><</a>
                        </li>
                        ";
        }
        // line 72
        echo "
                        ";
        // line 73
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["page_number"]) {
            // line 74
            echo "                            
                            <li class=\"";
            // line 75
            echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) == $context["page_number"])) ? ("active") : (null));
            echo "\"><a href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => $context["page_number"]));
            echo "\">";
            echo twig_escape_filter($this->env, $context["page_number"], "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page_number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "
                        ";
        // line 78
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()))) {
            // line 79
            echo "                        <li>
                            <a href=\"";
            // line 80
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) + 1)));
            echo "\">></a>
                        </li>
                        ";
        }
        // line 83
        echo "                        
                        ";
        // line 84
        if (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) >= 1) && (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) < (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) - 1)))) {
            // line 85
            echo "                        <li>
                            <a href=\"";
            // line 86
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
            echo "\">>></a>
                        </li>
                        ";
        }
        // line 89
        echo "
                    </ul>
                </nav>
                
            </div>
                <div class=\"row\" id=\"product_list\">
                    ";
        // line 95
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("products"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 96
        echo "                </div>
                   
                

                <div class=\"clearfix hidden-xs\"></div>
                <div class=\"clearfix\"></div>


                <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        ";
        // line 107
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 2)) {
            // line 108
            echo "                        <li>
                            <a href=\"";
            // line 109
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => 1));
            echo "\"><<</a>
                        </li>
                        ";
        }
        // line 112
        echo "                        
                        ";
        // line 113
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 1)) {
            // line 114
            echo "                        <li>
                            <a href=\"";
            // line 115
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) - 1)));
            echo "\"><</a>
                        </li>
                        ";
        }
        // line 118
        echo "
                        ";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["page_number"]) {
            // line 120
            echo "                            
                            <li class=\"";
            // line 121
            echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) == $context["page_number"])) ? ("active") : (null));
            echo "\"><a href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => $context["page_number"]));
            echo "\">";
            echo twig_escape_filter($this->env, $context["page_number"], "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page_number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "
                        ";
        // line 124
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()))) {
            // line 125
            echo "                        <li>
                            <a href=\"";
            // line 126
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) + 1)));
            echo "\">></a>
                        </li>
                        ";
        }
        // line 129
        echo "                        
                        ";
        // line 130
        if (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) >= 1) && (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) < (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) - 1)))) {
            // line 131
            echo "                        <li>
                            <a href=\"";
            // line 132
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
            echo "\">>></a>
                        </li>
                        ";
        }
        // line 135
        echo "
                    </ul>
                </nav>
                
            </div>

            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/produse-uri.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 135,  251 => 132,  248 => 131,  246 => 130,  243 => 129,  237 => 126,  234 => 125,  232 => 124,  229 => 123,  217 => 121,  214 => 120,  210 => 119,  207 => 118,  201 => 115,  198 => 114,  196 => 113,  193 => 112,  187 => 109,  184 => 108,  182 => 107,  169 => 96,  165 => 95,  157 => 89,  151 => 86,  148 => 85,  146 => 84,  143 => 83,  137 => 80,  134 => 79,  132 => 78,  129 => 77,  117 => 75,  114 => 74,  110 => 73,  107 => 72,  101 => 69,  98 => 68,  96 => 67,  93 => 66,  87 => 63,  84 => 62,  82 => 61,  21 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set categories = builderList.records %}
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div class=\"clearfix\">&nbsp;</div>

            <ul class=\"list-unstyled\" id=\"sidebar_subcategs\">
                <li>
                    <a href=\"atv-quad/can-am/1\">ATV Can-Am
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/cf-moto/1\">ATV CF Moto
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-honda/1\">ATV Honda
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-kawasaki/1\">ATV Kawasaki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-linhai/1\">ATV Linhai
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/polaris/1\">ATV Polaris
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-suzuki/1\">ATV Suzuki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-yamaha/1\">ATV Yamaha
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/can-am-ssv/1\">Can-Am SSV
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class=\"col-md-9\">
            <h1>ATV & Quad</h1>
            <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                    
                        {% if products.currentPage > 2 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: 1})}}\"><<</a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage > 1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage-1})}}\"><</a>
                        </li>
                        {% endif %}

                        {% for page_number in 1..products.lastPage %}
                            
                            <li class=\"{{products.currentPage == page_number ? 'active' : null }}\"><a href=\"{{this.page.baseFileName|page({ page: page_number})}}\">{{page_number}}</a></li>
                        {% endfor %}

                        {% if products.lastPage > products.currentPage %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage+1 })}}\">></a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage >= 1 and products.currentPage < products.lastPage-1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.lastPage})}}\">>></a>
                        </li>
                        {% endif %}

                    </ul>
                </nav>
                
            </div>
                <div class=\"row\" id=\"product_list\">
                    {% component 'products' %}
                </div>
                   
                

                <div class=\"clearfix hidden-xs\"></div>
                <div class=\"clearfix\"></div>


                <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        {% if products.currentPage > 2 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: 1})}}\"><<</a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage > 1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage-1})}}\"><</a>
                        </li>
                        {% endif %}

                        {% for page_number in 1..products.lastPage %}
                            
                            <li class=\"{{products.currentPage == page_number ? 'active' : null }}\"><a href=\"{{this.page.baseFileName|page({ page: page_number})}}\">{{page_number}}</a></li>
                        {% endfor %}

                        {% if products.lastPage > products.currentPage %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage+1 })}}\">></a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage >= 1 and products.currentPage < products.lastPage-1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.lastPage})}}\">>></a>
                        </li>
                        {% endif %}

                    </ul>
                </nav>
                
            </div>

            </div>
        </div>
    </div>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/produse-uri.htm", "");
    }
}
