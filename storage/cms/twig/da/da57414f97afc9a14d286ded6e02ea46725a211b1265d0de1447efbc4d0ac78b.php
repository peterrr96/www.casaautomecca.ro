<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/pages/side-by-side.htm */
class __TwigTemplate_a4026cdcf382f197ffe125b2de6f97e393c144c5243d0b4185db8755473779c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div class=\"clearfix\">&nbsp;</div>

            <ul class=\"list-unstyled\" id=\"sidebar_subcategs\">
                <li>
                    <a href=\"atv-quad/can-am\">CF MOTO
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/cf-moto\">KAWASAKI
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-honda\">Polaris
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-kawasaki\">YAMAHA
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-linhai\">Vehicule speciale
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                
            </ul>
        </div>
        <div class=\"col-md-9\">

            <h1>Side by side</h1>
            

            
            <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        
                        ";
        // line 45
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 2)) {
            // line 46
            echo "                        <li>
                            <a href=\"";
            // line 47
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => 1));
            echo "\"><<</a>
                        </li>
                        ";
        }
        // line 50
        echo "                        
                        ";
        // line 51
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 1)) {
            // line 52
            echo "                        <li>
                            <a href=\"";
            // line 53
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) - 1)));
            echo "\"><</a>
                        </li>
                        ";
        }
        // line 56
        echo "
                        ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["page_number"]) {
            // line 58
            echo "                            
                            <li class=\"";
            // line 59
            echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) == $context["page_number"])) ? ("active") : (null));
            echo "\"><a href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => $context["page_number"]));
            echo "\">";
            echo twig_escape_filter($this->env, $context["page_number"], "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page_number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "
                        ";
        // line 62
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()))) {
            // line 63
            echo "                        <li>
                            <a href=\"";
            // line 64
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) + 1)));
            echo "\">></a>
                        </li>
                        ";
        }
        // line 67
        echo "                        
                        ";
        // line 68
        if (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) >= 1) && (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) < (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) - 1)))) {
            // line 69
            echo "                        <li>
                            <a href=\"";
            // line 70
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
            echo "\">>></a>
                        </li>
                        ";
        }
        // line 73
        echo "
                    </ul>
                </nav>
                
            </div>
            
                   <div class=\"row\" id=\"product_list\">
                        ";
        // line 80
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("products"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 81
        echo "                    </div>
                   
                

                <div class=\"clearfix hidden-xs\"></div>
                <div class=\"clearfix\"></div>


                <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        ";
        // line 92
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 2)) {
            // line 93
            echo "                        <li>
                            <a href=\"";
            // line 94
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => 1));
            echo "\"><<</a>
                        </li>
                        ";
        }
        // line 97
        echo "                        
                        ";
        // line 98
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 1)) {
            // line 99
            echo "                        <li>
                            <a href=\"";
            // line 100
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) - 1)));
            echo "\"><</a>
                        </li>
                        ";
        }
        // line 103
        echo "
                        ";
        // line 104
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["page_number"]) {
            // line 105
            echo "                            
                            <li class=\"";
            // line 106
            echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) == $context["page_number"])) ? ("active") : (null));
            echo "\"><a href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => $context["page_number"]));
            echo "\">";
            echo twig_escape_filter($this->env, $context["page_number"], "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page_number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "
                        ";
        // line 109
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()))) {
            // line 110
            echo "                        <li>
                            <a href=\"";
            // line 111
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) + 1)));
            echo "\">></a>
                        </li>
                        ";
        }
        // line 114
        echo "                        
                        ";
        // line 115
        if (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) >= 1) && (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) < (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) - 1)))) {
            // line 116
            echo "                        <li>
                            <a href=\"";
            // line 117
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
            echo "\">>></a>
                        </li>
                        ";
        }
        // line 120
        echo "
                    </ul>
                </nav>
                
            </div>

            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/side-by-side.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 120,  235 => 117,  232 => 116,  230 => 115,  227 => 114,  221 => 111,  218 => 110,  216 => 109,  213 => 108,  201 => 106,  198 => 105,  194 => 104,  191 => 103,  185 => 100,  182 => 99,  180 => 98,  177 => 97,  171 => 94,  168 => 93,  166 => 92,  153 => 81,  149 => 80,  140 => 73,  134 => 70,  131 => 69,  129 => 68,  126 => 67,  120 => 64,  117 => 63,  115 => 62,  112 => 61,  100 => 59,  97 => 58,  93 => 57,  90 => 56,  84 => 53,  81 => 52,  79 => 51,  76 => 50,  70 => 47,  67 => 46,  65 => 45,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div class=\"clearfix\">&nbsp;</div>

            <ul class=\"list-unstyled\" id=\"sidebar_subcategs\">
                <li>
                    <a href=\"atv-quad/can-am\">CF MOTO
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/cf-moto\">KAWASAKI
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-honda\">Polaris
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-kawasaki\">YAMAHA
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-linhai\">Vehicule speciale
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                
            </ul>
        </div>
        <div class=\"col-md-9\">

            <h1>Side by side</h1>
            

            
            <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        
                        {% if products.currentPage > 2 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: 1})}}\"><<</a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage > 1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage-1})}}\"><</a>
                        </li>
                        {% endif %}

                        {% for page_number in 1..products.lastPage %}
                            
                            <li class=\"{{products.currentPage == page_number ? 'active' : null }}\"><a href=\"{{this.page.baseFileName|page({ page: page_number})}}\">{{page_number}}</a></li>
                        {% endfor %}

                        {% if products.lastPage > products.currentPage %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage+1 })}}\">></a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage >= 1 and products.currentPage < products.lastPage-1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.lastPage})}}\">>></a>
                        </li>
                        {% endif %}

                    </ul>
                </nav>
                
            </div>
            
                   <div class=\"row\" id=\"product_list\">
                        {% component 'products' %}
                    </div>
                   
                

                <div class=\"clearfix hidden-xs\"></div>
                <div class=\"clearfix\"></div>


                <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        {% if products.currentPage > 2 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: 1})}}\"><<</a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage > 1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage-1})}}\"><</a>
                        </li>
                        {% endif %}

                        {% for page_number in 1..products.lastPage %}
                            
                            <li class=\"{{products.currentPage == page_number ? 'active' : null }}\"><a href=\"{{this.page.baseFileName|page({ page: page_number})}}\">{{page_number}}</a></li>
                        {% endfor %}

                        {% if products.lastPage > products.currentPage %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage+1 })}}\">></a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage >= 1 and products.currentPage < products.lastPage-1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.lastPage})}}\">>></a>
                        </li>
                        {% endif %}

                    </ul>
                </nav>
                
            </div>

            </div>
        </div>
    </div>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/side-by-side.htm", "");
    }
}
