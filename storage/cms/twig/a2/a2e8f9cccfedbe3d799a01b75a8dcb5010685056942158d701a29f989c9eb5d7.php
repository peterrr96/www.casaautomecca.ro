<?php

/* E:\Dyborian1\www.casaautomecca.ro/themes/mecca/pages/can-am.htm */
class __TwigTemplate_baca0c03be117b416932a3fbf3eb548cc6b96bcc4fdef945c02bab5dbbd4f87e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div class=\"clearfix\">&nbsp;</div>

            <ul class=\"list-unstyled\" id=\"sidebar_subcategs\">
                <li>
                    <a href=\"atv-quad/can-am\">ATV Can-Am
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/cf-moto\">ATV CF Moto
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-honda\">ATV Honda
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-kawasaki\">ATV Kawasaki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-linhai\">ATV Linhai
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/polaris\">ATV Polaris
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-suzuki\">ATV Suzuki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-yamaha\">ATV Yamaha
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/can-am-ssv\">Can-Am SSV
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class=\"col-md-9\">
            <h1>ATV Can-Am</h1>
            <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                    
                        ";
        // line 60
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 2)) {
            // line 61
            echo "                        <li>
                            <a href=\"";
            // line 62
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => 1));
            echo "\"><<</a>
                        </li>
                        ";
        }
        // line 65
        echo "                        
                        ";
        // line 66
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 1)) {
            // line 67
            echo "                        <li>
                            <a href=\"";
            // line 68
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) - 1)));
            echo "\"><</a>
                        </li>
                        ";
        }
        // line 71
        echo "
                        ";
        // line 72
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["page_number"]) {
            // line 73
            echo "                            
                            <li class=\"";
            // line 74
            echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) == $context["page_number"])) ? ("active") : (null));
            echo "\"><a href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => $context["page_number"]));
            echo "\">";
            echo twig_escape_filter($this->env, $context["page_number"], "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page_number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "
                        ";
        // line 77
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()))) {
            // line 78
            echo "                        <li>
                            <a href=\"";
            // line 79
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) + 1)));
            echo "\">></a>
                        </li>
                        ";
        }
        // line 82
        echo "                        
                        ";
        // line 83
        if (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) >= 1) && (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) < (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) - 1)))) {
            // line 84
            echo "                        <li>
                            <a href=\"";
            // line 85
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
            echo "\">>></a>
                        </li>
                        ";
        }
        // line 88
        echo "
                    </ul>
                </nav>
                
            </div>
                <div class=\"row\" id=\"product_list\">
                    ";
        // line 94
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("products"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 95
        echo "                </div>
                   
                

                <div class=\"clearfix hidden-xs\"></div>
                <div class=\"clearfix\"></div>


                <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        ";
        // line 106
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 2)) {
            // line 107
            echo "                        <li>
                            <a href=\"";
            // line 108
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => 1));
            echo "\"><<</a>
                        </li>
                        ";
        }
        // line 111
        echo "                        
                        ";
        // line 112
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) > 1)) {
            // line 113
            echo "                        <li>
                            <a href=\"";
            // line 114
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) - 1)));
            echo "\"><</a>
                        </li>
                        ";
        }
        // line 117
        echo "
                        ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["page_number"]) {
            // line 119
            echo "                            
                            <li class=\"";
            // line 120
            echo (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) == $context["page_number"])) ? ("active") : (null));
            echo "\"><a href=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => $context["page_number"]));
            echo "\">";
            echo twig_escape_filter($this->env, $context["page_number"], "html", null, true);
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page_number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "
                        ";
        // line 123
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()))) {
            // line 124
            echo "                        <li>
                            <a href=\"";
            // line 125
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) + 1)));
            echo "\">></a>
                        </li>
                        ";
        }
        // line 128
        echo "                        
                        ";
        // line 129
        if (((twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) >= 1) && (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "currentPage", array()) < (twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array()) - 1)))) {
            // line 130
            echo "                        <li>
                            <a href=\"";
            // line 131
            echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "baseFileName", array()), array("page" => twig_get_attribute($this->env, $this->getSourceContext(), ($context["products"] ?? null), "lastPage", array())));
            echo "\">>></a>
                        </li>
                        ";
        }
        // line 134
        echo "
                    </ul>
                </nav>
                
            </div>

            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/can-am.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 134,  249 => 131,  246 => 130,  244 => 129,  241 => 128,  235 => 125,  232 => 124,  230 => 123,  227 => 122,  215 => 120,  212 => 119,  208 => 118,  205 => 117,  199 => 114,  196 => 113,  194 => 112,  191 => 111,  185 => 108,  182 => 107,  180 => 106,  167 => 95,  163 => 94,  155 => 88,  149 => 85,  146 => 84,  144 => 83,  141 => 82,  135 => 79,  132 => 78,  130 => 77,  127 => 76,  115 => 74,  112 => 73,  108 => 72,  105 => 71,  99 => 68,  96 => 67,  94 => 66,  91 => 65,  85 => 62,  82 => 61,  80 => 60,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div class=\"clearfix\">&nbsp;</div>

            <ul class=\"list-unstyled\" id=\"sidebar_subcategs\">
                <li>
                    <a href=\"atv-quad/can-am\">ATV Can-Am
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/cf-moto\">ATV CF Moto
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-honda\">ATV Honda
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-kawasaki\">ATV Kawasaki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-linhai\">ATV Linhai
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/polaris\">ATV Polaris
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-suzuki\">ATV Suzuki
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/atv-yamaha\">ATV Yamaha
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
                <li>
                    <a href=\"atv-quad/can-am-ssv\">Can-Am SSV
                        <i class=\"fa fa-caret-right\"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class=\"col-md-9\">
            <h1>ATV Can-Am</h1>
            <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                    
                        {% if products.currentPage > 2 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: 1})}}\"><<</a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage > 1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage-1})}}\"><</a>
                        </li>
                        {% endif %}

                        {% for page_number in 1..products.lastPage %}
                            
                            <li class=\"{{products.currentPage == page_number ? 'active' : null }}\"><a href=\"{{this.page.baseFileName|page({ page: page_number})}}\">{{page_number}}</a></li>
                        {% endfor %}

                        {% if products.lastPage > products.currentPage %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage+1 })}}\">></a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage >= 1 and products.currentPage < products.lastPage-1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.lastPage})}}\">>></a>
                        </li>
                        {% endif %}

                    </ul>
                </nav>
                
            </div>
                <div class=\"row\" id=\"product_list\">
                    {% component 'products' %}
                </div>
                   
                

                <div class=\"clearfix hidden-xs\"></div>
                <div class=\"clearfix\"></div>


                <div>
                <nav style=\"text-align: center;\">
                    <ul class=\"pagination\" style=\"margin-top: 0;\">
                        {% if products.currentPage > 2 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: 1})}}\"><<</a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage > 1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage-1})}}\"><</a>
                        </li>
                        {% endif %}

                        {% for page_number in 1..products.lastPage %}
                            
                            <li class=\"{{products.currentPage == page_number ? 'active' : null }}\"><a href=\"{{this.page.baseFileName|page({ page: page_number})}}\">{{page_number}}</a></li>
                        {% endfor %}

                        {% if products.lastPage > products.currentPage %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.currentPage+1 })}}\">></a>
                        </li>
                        {% endif %}
                        
                        {% if products.currentPage >= 1 and products.currentPage < products.lastPage-1 %}
                        <li>
                            <a href=\"{{ this.page.baseFileName|page({ page: products.lastPage})}}\">>></a>
                        </li>
                        {% endif %}

                    </ul>
                </nav>
                
            </div>

            </div>
        </div>
    </div>", "E:\\Dyborian1\\www.casaautomecca.ro/themes/mecca/pages/can-am.htm", "");
    }
}
